# LeadBop - Overview

LeadBop is a central repository of business data already available on the web. We are a Data Science company acting as a lead generation platform. LeadBop provides a simple method of tailored data collection for any budge.

LeadBop is a joint effort between 1 front end developer, 1 UX designer, and 2 data scientists (I am one of these data scientists). I collaborated with the other data scientist to built the backend for this project. We built the backend using python and firebase (as our data base). Essentially, the backend scrapes the web to generate leads for general businesses. You are able to type in any business type, any city (in Canada and the USA) and it will return a list of business names, addresses, and phone numbers in the city. Below you will find the link for LeadBop if you are interested in how it works. Unfortunately, the code is proprietary so I am unable to share the code here but if you have any questions or inquries please feel free to reach out to me at davidmammarella12@gmail.com.

LeadBop link: https://www.leadbop.com/ 



